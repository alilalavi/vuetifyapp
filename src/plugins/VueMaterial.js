import Vue from 'vue';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import {
  MdButton,
  MdContent,
  MdTabs,
  MdApp,
  MdBottomBar,
  MdCard,
  MdCheckbox,
  MdChips,
  MdDatepicker,
  MdDialog,
  MdDivider,
  MdDrawer,
  MdElevation,
  MdEmptyState,
  MdField,
  MdHighlightText,
  MdIcon,
  MdImage,
  MdLayout,
  MdList,
  MdMenu,
  MdProgress,
  MdRadio,
  MdRipple,
  MdSnackbar,
  MdSpeedDial,
  MdToolbar,
  MdSteppers,
  MdSubheader,
  MdSwitch,
  MdTable,
  MdAutocomplete,
  MdAvatar,
  MdBadge,
  MdTooltip
} from 'vue-material/dist/components';
Vue.use(MdTable);
Vue.use(MdTooltip);
Vue.use(MdAutocomplete);
Vue.use(MdAvatar);
Vue.use(MdBadge);
Vue.use(MdSteppers);
Vue.use(MdSubheader);
Vue.use(MdRadio);
Vue.use(MdSwitch);
Vue.use(MdRadio);
Vue.use(MdRipple);
Vue.use(MdSnackbar);
Vue.use(MdSpeedDial);
Vue.use(MdToolbar);
Vue.use(MdIcon);
Vue.use(MdImage);
Vue.use(MdLayout);
Vue.use(MdList);
Vue.use(MdDivider);
Vue.use(MdDivider);
Vue.use(MdMenu);
Vue.use(MdProgress);
Vue.use(MdButton);
Vue.use(MdContent);
Vue.use(MdTabs);
Vue.use(MdApp);
Vue.use(MdBottomBar);
Vue.use(MdCard);
Vue.use(MdCheckbox);
Vue.use(MdChips);
Vue.use(MdDatepicker);
Vue.use(MdDialog);
Vue.use(MdDivider);
Vue.use(MdDrawer);
Vue.use(MdElevation);
Vue.use(MdEmptyState);
Vue.use(MdField);
Vue.use(MdHighlightText);
Vue.use(VueMaterial, {
  rtl: true
});