import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import fa from 'vuetify/lib/locale/fa';
import './imports';

const MY_ICONS = {
  complete: '...',
  cancel: '...',
  close: '...',
  delete: '...', // delete (e.g. v-chip close)
  clear: '...',
  success: '...',
  info: '...',
  warning: '...',
  error: '...',
  prev: '...',
  next: '...',
  checkboxOn: '...',
  checkboxOff: '...',
  checkboxIndeterminate: '...',
  delimiter: '...', // for carousel
  sort: '...',
  expand: '...',
  menu: '...',
  subgroup: '...',
  dropdown: '...',
  radioOn: '...',
  radioOff: '...',
  edit: '...',
  ratingEmpty: '...',
  ratingFull: '...',
  ratingHalf: '...',
  loading: '...',
  first: '...',
  last: '...',
  unfold: '...',
  file: '...',
};

Vue.use(Vuetify, {
  rtl: true,
  lang: {
    locales: {
      fa
    },
    current: 'fa',
  },
  icons: {
    iconfont: 'mdiSvg , mdi , md , fa , fa4 , faSvg',
    // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
    values: {
      MY_ICONS,
      product: 'mdi-dropbox',
      support: 'mdi-lifebuoy',
      steam: 'mdi-steam-box',
      pc: 'mdi-desktop-classic',
      xbox: 'mdi-xbox',
      playstation: 'mdi-playstation',
      switch: 'mdi-nintendo-switch',
    },
  },
});