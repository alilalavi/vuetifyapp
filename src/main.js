import Vue from 'vue'
import './plugins/axios'
import './plugins/vuetify'
import App from './App.vue'
import router from './router/router'
import store from './store/store'
import './plugins/vuesax.js'
import './plugins/VueMaterial'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';
import {
  library
} from '@fortawesome/fontawesome-svg-core';
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome';
import {
  faHome,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt
} from '@fortawesome/free-solid-svg-icons';
library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt);
Vue.config.productionTip = false;
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;
Vue.use(VuePersianDatetimePicker, {
  name: 'pdatepicker',
  props: {
    inputFormat: 'YYYY-MM-DD',
    format: 'jYYYY-jMM-jDD',
    editable: false,
    inputClass: 'form-control md-4',
    placeholder: 'لطفا تاریخ تولد خود را وارد کنید',
    altFormat: 'YYYY-MM-DD',
    color: '#00acc1',
    autoSubmit: false,
  }
});



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');