import isScreen from '../core/screenHelper.js';

export const MessageStates = {
  READ: "reed",
  NEW: "new",
  HIDDEN: "hidden"
};

Object.freeze(MessageStates);

export default {
  namespaced: true,
  state: {
    sidebarClose: false,
    sidebarStatic: false,
    sidebarActiveElement: null,
  },
  mutations: {
    toggleSidebar(state) {
      const nextState = !state.sidebarStatic;

      localStorage.sidebarStatic = nextState;
      state.sidebarStatic = nextState;

      if (!nextState && (isScreen('lg') || isScreen('xl'))) {
        state.sidebarClose = true;
      }
    },
    switchSidebar(state, value) {
      if (value) {
        state.sidebarClose = value;
      } else {
        state.sidebarClose = !state.sidebarClose;
      }
    },
  },
  actions: {
    toggleSidebar({
      commit
    }) {
      commit('toggleSidebar')
    },
    switchSidebar({
      commit
    }, value) {
      commit('switchSidebar', value)
    }
  }
}